#j2cache-shiro

实现Shiro的CacheManager,使其能使用J2Cache作为session缓存

## 配置方法

shiro.ini 中加入下面配置项即可

```
cacheManager = org.nutz.j2cache.shiro.J2CacheManager
```

## Maven依赖

```
<dependency>
	<groupId>org.nutz</groupId>
	<artifactId>j2cache-shiro</artifactId>
	<version>1.0.2</version>
</dependency>
```